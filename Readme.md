# RECALIFICACIÓN SOCIO ECONÓMICA Y CULTURAL DEL CENTRO HISTÓRICO DE SAN SALVADOR Y DE SU FUNCIÓN HABITACIONAL MEDIANTE EL MOVIMIENTO COOPERATIVO

La iniciativa se desarrolla sobre dos ejes principales: la resolución de las problemáticas del Centro Histórico de la 
ciudad de San Salvador, a través de la recuperación de la propia función habitacional, social y estructural; 
y la promoción de las Cooperativas Habitacionales. Ambos ejes se entrelazan para contribuir a la solución de la 
problemática de vivienda de las familias con escasos recursos financieros, que viven en condiciones de precariedad y 
que mayoritariamente trabajan en el Centro Histórico. [articulo](http://coopit-acc.org/recalificacion-socio-economica-y-cultural-del-centro-historico-de-san-salvador-y-funcion-habitacional-mediante-el-movimiento-cooperativo/)

## Getting Started

- Clone git clone ```https://alankvm@bitbucket.org/alankvm/sshistoriccenter.git```
- Open with Rider / VSCode / Visual Studio 

### Prerequisites

- Git
- asp.net core 2.1 SDK & runtime
- internet connection


## Running the tests

- Open project solution
- run ```dotnet test``` in your favorite terminal

## Commands
Controllers

- ```dotnet aspnet-codegenerator controller  -name BookController  -async -m Book -dc AccountMe.Models.AppData.AppDbContext  -udl -outDir Controllers```
- ```dotnet aspnet-codegenerator controller  -name CompanyController  -async -m Company -dc AccountMe.Models.AppData.AppDbContext  -udl -outDir Controllers```
- ```dotnet aspnet-codegenerator controller  -name ChartAccounts  -async -m ChartAccount -dc AccountMe.Models.AppData.AppDbContext  -udl -outDir Controllers```
- ```dotnet aspnet-codegenerator controller  -name AccountTypes  -async -m AccountType -dc AccountMe.Models.AppData.AppDbContext  -udl -outDir Controllers```
- ```dotnet aspnet-codegenerator controller  -name Accounts  -async -m Account -dc AccountMe.Models.AppData.AppDbContext  -udl -outDir Controllers```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Nuget](https://www.nuget.org/) - NuGet is the package manager for .NET. The NuGet client tools provide the ability to produce and consume packages. The NuGet Gallery is the central package repository used by all package authors and consumers.
* [ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.1) - ASP.NET Core is a cross-platform, high-performance, open-source framework for building modern, cloud-based, Internet-connected applications. 
* [xUnit](https://xunit.net/) - xUnit.net is a free, open source, community-focused unit testing tool for the .NET Framework.

## Authors

* **Alan Alvarenga** - Developer


## Acknowledgments

* This project is my two cents collaboration for this awesome initiative lead by my good friend Gustavo Milan.

