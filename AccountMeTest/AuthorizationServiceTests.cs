using System;
using System.Collections.Generic;
using AccountMe.Models;
using AccountMe.Services;
using AccountMe.Utils;
using Xunit;
using Xunit.Abstractions;

namespace AccountMeTest
{
    public class AuthorizationServiceTests
    {
        private readonly AuthorizationService _as;
        private readonly ITestOutputHelper _output;

        public AuthorizationServiceTests(ITestOutputHelper output)
        {
            _output = output;
            _as = new AuthorizationService();
            _setMockUpUsers();
        }

        private void _setMockUpUsers()
        {
            _output.WriteLine("Set up mockup users");
            _as.Users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Role = new Role(),
                    Email = "alankvm@gmail.com",
                    Active = true,
                    Created = DateTime.Now,
                    Manager = null,
                    Updated = DateTime.Now,
                    Password = Security.Hash("nekstd54!"),
                    IdRole = 1,
                    KnownAs = "Alan",
                    FullName = "Alan Alvarenga",
                    CreatedBy = "admin",
                    UpdatedBy = null
                }
            };
        }

        [Theory]
        [InlineData("alankvm@gmail.com", "123456")]
        public void IsValidUserFalse(string u, string p)
        {
            _output.WriteLine("IsValidUserFalse");
            Assert.False(_as.IsValid(u, p));
        }

        [Theory]
        [InlineData("alankvm@gmail.com", "nekstd54!")]
        public void IsValidUserTrue(string u, string p)
        {
            _output.WriteLine("IsValidUserTrue");
            _output.WriteLine("User {0}", u);
            _output.WriteLine("Password {0}", p);
            _output.WriteLine("Password HASH {0}", Security.Hash(p));
            Assert.True(_as.IsValid(u, p));
        }

        [Theory]
        [InlineData(1)]
        public void IsValidUserIdEqualsTo(int id)
        {
            _as.IsValid("alankvm@gmail.com", "nekstd54!");
            Assert.Equal(id, _as.GetCurrentUser().Id);
        }

        [Theory]
        [InlineData("alankvm@gmail.com", "123456")]
        public void IsUserNull(string u, string p)
        {
            _as.IsValid(u, p);
            var user = _as.GetCurrentUser();
            Assert.Null(user);
        }
    }
}