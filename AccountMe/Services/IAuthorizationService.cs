using System.Threading.Tasks;
using AccountMe.Models;
using Microsoft.AspNetCore.Http;

namespace AccountMe.Services
{
    /// <summary>
    ///     Authorization method definition
    /// </summary>
    public interface IAuthorizationService
    {
        User Login(string email, string pwd);
        User GetCurrentUser();
        Task SetCookie(HttpContext context);
    }
}