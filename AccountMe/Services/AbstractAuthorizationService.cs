using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AccountMe.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;

namespace AccountMe.Services
{
    /// <summary>
    ///     Authorization implementation
    /// </summary>
    public abstract class AbstractAuthorizationService : IAuthorizationService
    {
        protected User CurrentUser { get; set; }
        public List<User> Users { get; set; }

        /// <summary>
        ///     get registered user if exists
        /// </summary>
        /// <param name="email">user email</param>
        /// <param name="pwd">user password</param>
        /// <returns>Registered user instance</returns>
        public virtual User Login(string email, string pwd)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(pwd)) return null;
            return Users.FirstOrDefault(u => u.Email == email && u.Password == pwd);
        }

        /// <summary>
        ///     If login is successful a valid user will be return
        /// </summary>
        /// <returns></returns>
        public virtual User GetCurrentUser()
        {
            return CurrentUser;
        }

        /// <summary>
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException"></exception>
        public virtual async Task SetCookie(HttpContext context)
        {
            if (GetCurrentUser() == null) throw new NullReferenceException("Invalid user reference");
            var claims = _getClaims();
            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);
            await context
                .SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
        }

        /// <summary>
        ///     Set user claims
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerable<Claim> _getClaims()
        {
            var user = GetCurrentUser();
            IEnumerable<Claim> claims = new[]
            {
                new Claim(ClaimTypes.Name, user.KnownAs),
                new Claim(ClaimTypes.GivenName, user.UserName),
                new Claim(ClaimTypes.Role, user.Role.Name),
                new Claim(ClaimTypes.Sid, user.Id.ToString()),
            };
            return claims;
        }
    }
}