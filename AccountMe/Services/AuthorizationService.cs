using System.Linq;
using AccountMe.Models.AppData;
using AccountMe.Utils;
using Microsoft.EntityFrameworkCore;

namespace AccountMe.Services
{
    /// <summary>
    ///     Application authorization service
    /// </summary>
    public class AuthorizationService : AbstractAuthorizationService
    {
        /// <summary>
        ///     Zero Param Initializer
        /// </summary>
        public AuthorizationService()
        {
            var db = new AppDbContext();
            Users = db.Users.ToList();
        }
        
        /// <summary>
        /// Inject AppDbContext 
        /// </summary>
        /// <param name="db"></param>
        public AuthorizationService(AppDbContext db)
        {
            Users = db.Users.Include(s => s.Role).Where(s => s.Active).ToList();
        }

        /// <summary>
        ///     Check if user is valid
        /// </summary>
        /// <param name="email">user's registered email</param>
        /// <param name="pwd">user's password</param>
        /// <returns></returns>
        public bool IsValid(string email, string pwd)
        {
            CurrentUser = Login(email, Security.Hash(pwd));
            return CurrentUser != null;
        }
    }
}