using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountMe.Models
{
    /// <summary>
    ///     Journal registry for transaction log
    /// </summary>
    public class Book : Audit
    {
        [Key] public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, defina un nombre para su libro")]
        [MinLength(5, ErrorMessage = "El nombre del libro debe ser más específico")]
        [StringLength(100), Display(Name = "Libro", Prompt = "Ej. Periodo 2019")]
        public string Name { get; set; }

        [Required, Display(Name = "Persona o Razón Social")]
        public int IdCompany { get; set; }

        [Display(Name = "Sólo lectura")] public bool ReadOnly { get; set; } = false;

        [Required(ErrorMessage = "Por favor, especifica el periodo al que el libro aplica"), Display(Name = "Periodo")]
        public int Period { get; set; }

        [Display(Name = "Comentarios"), DataType(DataType.MultilineText)]
        public string Comment { get; set; }
        //TODO: Add NIT / DUI ask accountant

        #region ForeignKeys

        [ForeignKey("IdCompany")] public virtual Company Company { get; set; }

        #endregion
    }
}