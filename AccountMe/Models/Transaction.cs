using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AccountMe.Utils;

namespace AccountMe.Models
{
    /// <summary>
    ///     Granular log to complete the journal with all transaction entered to a particular book
    /// </summary>
    public class Transaction : Audit
    {
        [Key] public int Id { get; set; }

        [Display(Name = "Tipo de transaccion", Prompt = "Tipo de transaccion")]
        public int IdTransactionType { get; set; }

        [Display(Name = "Tipo de balance", Prompt = "Tipo de balance")]
        public int IdBalanceType { get; set; }

        [Display(Name = "Total", Prompt = "Total")]
        [DataType(DataType.Currency)]
        public decimal Total { get; set; }

        [Display(Name = "Sub Total", Prompt = "Sub Total")]
        [DataType(DataType.Currency)]
        public decimal SubTotal { get; set; }

        [Display(Name = "IVA", Prompt = "IVA")]
        [DataType(DataType.Currency)]
        public decimal? Tax { get; set; } = ConstantValue.Zero;

        [Display(Name = "Retencion 1%", Prompt = "Retencion 1%")]
        [DataType(DataType.Currency)]
        public decimal? Retention { get; set; } = ConstantValue.Zero;

        [Display(Name = "Book", Prompt = "Book")]
        public int IdBook { get; set; }

        #region ForeignKey

        [ForeignKey("IdTransactionType")] public virtual TransactionType TransactionType { get; set; }
        [ForeignKey("IdBalanceType")] public virtual BalanceType BalanceType { get; set; }
        [ForeignKey("IdBook")] public virtual Book Book { get; set; }

        #endregion
    }
}