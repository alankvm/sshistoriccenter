using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountMe.Models
{
    /// <summary>
    ///     User Access Control
    /// </summary>
    public class User : Audit
    {
        [Key] public int Id { get; set; }

        [StringLength(200)]
        [Required]
        [Display(Name = "Nombre Completo", Prompt = "Ej. Alan K. Alvarenga")]
        public string FullName { get; set; }

        [StringLength(70)]
        [Display(Name = "Titulo", Prompt = "Ej. Ing. Alvarenga")]
        public string KnownAs { get; set; }

        [Required, StringLength(50), Display(Name = "Usuario Sistema")]
        public string UserName { get; set; }

        [StringLength(190)]
        [Required]
        [Display(Name = "Correo", Prompt = "Ej. nombre@gmail.com")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [StringLength(65)]
        [Required]
        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required] [Display(Name = "Perfil")] public int IdRole { get; set; }

        [Display(Name = "Activo?")] public bool Active { get; set; } = true;

        [Display(Name = "Superior")] public int? IdManager { get; set; }

        [ForeignKey("IdRole")] public virtual Role Role { get; set; }

        [ForeignKey("IdManager")] public virtual User Manager { get; set; }

        public virtual ICollection<User> Employees { get; set; }
        public virtual ICollection<Company> Companies { get; set; }
    }
}