using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AccountMe.Models
{
    /// <summary>
    ///     Application role access level
    /// </summary>
    public class Role : Audit
    {
        [Key] public int Id { get; set; }

        [StringLength(50)]
        [Required]
        [Display(Name = "Perfil")]
        public string Name { get; set; }

        [StringLength(240)]
        [Display(Name = "Descripción")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public virtual IEnumerable<User> Users { get; set; }
    }
}