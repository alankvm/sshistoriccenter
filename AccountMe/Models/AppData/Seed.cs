using System;
using AccountMe.Utils;
using Microsoft.EntityFrameworkCore;

namespace AccountMe.Models.AppData
{
    public class Seed
    {
        private ModelBuilder _modelBuilder;

        public Seed(ModelBuilder mb)
        {
            _modelBuilder = mb;
        }

        public void Init()
        {
            _seedUsers();
            _seedRoles();
            _seedBalanceTypes();
            _seedTransactionTypes();
        }

        private void _seedRoles()
        {
            _modelBuilder.Entity<Role>().HasData(new Role
            {
                Id = 1,
                Name = "SuperAdmin",
                Created = DateTime.Now,
                Updated = DateTime.Now,
                CreatedBy = "alan.alvarenga",
                UpdatedBy = "alan.alvarenga",
                Description = "Full administrative power"
            }, new Role
            {
                Id = 2,
                Name = "MainAccountant",
                Created = DateTime.Now,
                Updated = DateTime.Now,
                CreatedBy = "alan.alvarenga",
                UpdatedBy = "alan.alvarenga",
                Description = "Manager role"
            }, new Role
            {
                Id = 3,
                Name = "Accountant",
                Created = DateTime.Now,
                Updated = DateTime.Now,
                CreatedBy = "alan.alvarenga",
                UpdatedBy = "alan.alvarenga",
                Description = "Limited user role"
            });
        }

        private void _seedTransactionTypes()
        {
            _modelBuilder.Entity<TransactionType>().HasData(new TransactionType
            {
                Id = 1,
                Name = "Consumidor Final",
                CreatedBy = "alan.alvarenga",
                UpdatedBy = "alan.alvarenga"
            }, new TransactionType
            {
                Id = 2,
                Name = "Credito Fiscal",
                CreatedBy = "alan.alvarenga",
                UpdatedBy = "alan.alvarenga"
            });
        }

        private void _seedBalanceTypes()
        {
            _modelBuilder.Entity<BalanceType>().HasData(new BalanceType
            {
                Id = 1,
                Name = "Debe",
                CreatedBy = "alan.alvarenga",
                UpdatedBy = "alan.alvarenga"
            }, new BalanceType
            {
                Id = 2,
                Name = "Haber",
                CreatedBy = "alan.alvarenga",
                UpdatedBy = "alan.alvarenga"
            });
        }

        private void _seedUsers()
        {
            _modelBuilder.Entity<User>().HasData(new User
            {
                Id = 1,
                Email = "alankvm@gmail.com",
                Active = true,
                Created = DateTime.Now,
                Updated = DateTime.Now,
                Password = Security.Hash("nekstd54!"),
                IdRole = 1,
                KnownAs = "Ing. Alvarenga",
                FullName = "Alan Kevin Alvarenga Mejia",
                CreatedBy = "alan.alvarenga",
                UpdatedBy = "alan.alvarenga",
                UserName = "alan.alvarenga"
            });
        }
    }
}