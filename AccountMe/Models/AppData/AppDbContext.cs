using Microsoft.EntityFrameworkCore;

namespace AccountMe.Models.AppData
{
    public class AppDbContext : DbContext
    {
        /// <summary>
        ///     User defined roles
        /// </summary>
        public DbSet<Role> Roles { get; set; }

        /// <summary>
        ///     Users
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        ///     User's client companies
        /// </summary>
        public DbSet<Company> Companies { get; set; }

        /// <summary>
        ///     companies books
        /// </summary>
        public DbSet<Book> Books { get; set; }

        /// <summary>
        ///     transactional accounts types
        /// </summary>
        public DbSet<AccountType> AccountTypes { get; set; }

        /// <summary>
        ///     user defined chart accounts
        /// </summary>
        public DbSet<ChartAccount> ChartAccounts { get; set; }

        /// <summary>
        ///     user defined accounts
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        ///     operation balance
        /// </summary>
        public DbSet<BalanceType> BalanceTypes { get; set; }

        /// <summary>
        ///     type of transactions
        /// </summary>
        public DbSet<TransactionType> TransactionTypes { get; set; }

        /// <summary>
        ///     transaction list
        /// </summary>
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
                optionsBuilder.UseMySql(
                    "server=167.99.178.165;uid=alan;pwd=71B2ECC75F;database=DbAccounting;charset=utf8mb4;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            new Seed(modelBuilder).Init();
        }
    }
}