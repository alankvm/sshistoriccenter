using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountMe.Models
{
    /// <summary>
    ///     Customer Chart Account, this class enables each client to customize their journal with the
    ///     right accounts corresponding to the business nature
    /// </summary>
    public class ChartAccount : Audit
    {
        [Key] public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, defina un nombre para su catalogo de cuentas")]
        [Display(Name = "Catalogo de cuentas", Prompt = "Catalogo de cuentas")]
        public string Name { get; set; }

        [Display(Name = "Nombre / Razon Social a la que pertenece")]
        public int IdCompany { get; set; }

        #region ForeignKeys

        [ForeignKey("IdCompany")] public virtual Company Company { get; set; }
        public ICollection<Account> Accounts { get; set; }

        #endregion
    }
}