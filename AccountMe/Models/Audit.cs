using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountMe.Models
{
    [NotMapped]
    public abstract class Audit
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "Fecha/Hora Registro")]
        public DateTime Created { get; set; } = DateTime.Now;

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Fecha/Hora Actualizado")]
        public DateTime Updated { get; set; } = DateTime.Now;

        [Display(Name = "Registrado Por")]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        [Display(Name = "Actualizado Por")]
        [StringLength(50)]
        public string UpdatedBy { get; set; }
    }
}