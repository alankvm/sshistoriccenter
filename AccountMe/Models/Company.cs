using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountMe.Models
{
    /// <summary>
    ///     Customer information 
    /// </summary>
    public class Company : Audit
    {
        [Key] public int Id { get; set; }

        [StringLength(200)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, ingrese Nombre / Razón Social")]
        [Display(Name = "Nombre / Razón Social", Prompt = "Nombre / Razón Social")]
        public string Name { get; set; }

        [StringLength(250)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, ingrese Dirección")]
        [Display(Name = "Dirección", Prompt = "Dirección")]
        public string Address { get; set; }

        [StringLength(12)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, ingrese un número de teléfono")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Teléfono", Prompt = "Teléfono")]
        public string Phone { get; set; }

        [StringLength(150)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, ingrese un correo electronico")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Correo", Prompt = "Teléfono")]
        public string Email { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Comentarios")]
        public string Comment { get; set; }

        [Display(Name = "Contador Principal", Prompt = "Contador Principal")]
        public int IdUser { get; set; }

        #region ForeignKeys

        [ForeignKey("IdUser")] public virtual User MainAccountant { get; set; }

        public virtual ICollection<Book> Books { get; set; }
        public virtual ICollection<ChartAccount> ChartAccounts { get; set; }

        #endregion
    }
}