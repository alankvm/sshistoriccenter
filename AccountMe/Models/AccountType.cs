using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AccountMe.Models
{
    /// <summary>
    ///     Represents the different types of economic resources owned by each company
    /// </summary>
    public class AccountType : Audit
    {
        [Key] public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, defina un nombre para su tipo de cuenta")]
        public string Name { get; set; }

        #region ForeignKeys

        public virtual ICollection<Account> Accounts { get; set; }

        #endregion
    }
}