using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AccountMe.Models
{
    /// <summary>
    ///     Determine whether the transaction should be elegible to IRS Like operations
    /// </summary>
    public class TransactionType : Audit
    {
        [Key] public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, debes especificar un tipo de transaccion")]
        [Display(Name = "Tipo Transaccion", Prompt = "Tipo Transaccion")]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}