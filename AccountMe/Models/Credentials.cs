using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountMe.Models
{
    [NotMapped]
    public class Credentials
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Correo requierido")]
        [StringLength(150), DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Contraseña requerida")]
        [StringLength(65), DataType(DataType.Password)]
        public string Password { get; set; }
    }
}