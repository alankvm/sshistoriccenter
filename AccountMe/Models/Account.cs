using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountMe.Models
{
    /// <summary>
    ///     Hierarchical structure for account nesting in a parent-child relationship
    ///     all accounts must be registered with a parent chart account
    /// </summary>
    public class Account : Audit
    {
        [Key] public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, define un nombre para la cuenta")]
        [StringLength(120), Display(Name = "Cuenta", Prompt = "Cuenta")]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, defina un codigo de cuenta")]
        [Display(Name = "Codigo de cuenta", Prompt = "Codigo de cuenta")]
        public int Code { get; set; }

        [Display(Name = "Solo lectura", Prompt = "Solo lectura", Description = "No cargable o debitable")]
        public bool IsReadOnly { get; set; } = false;

        [Display(Name = "Cuenta Padre")] public int? IdParent { get; set; }
        [Display(Name = "Catalogo")] public int IdChartAccount { get; set; }
        [Display(Name = "Tipo de cuenta")] public int IdAccountType { get; set; }

        #region ForeignKeys

        [ForeignKey("IdParent")] public virtual Account ParentAccount { get; set; }
        [ForeignKey("IdChartAccount")] public virtual ChartAccount ChartAccount { get; set; }
        [ForeignKey("IdAccountType")] public virtual AccountType AccountType { get; set; }
        public virtual ICollection<Account> Children { get; set; }

        #endregion
    }
}