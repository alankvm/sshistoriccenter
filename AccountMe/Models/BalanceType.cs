using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AccountMe.Models
{
    /// <summary>
    ///     Balance types are used to categorize types of transactions registered in dailybook
    /// </summary>
    public class BalanceType : Audit
    {
        [Key] public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Por favor, debes especificar un tipo de balance")]
        [Display(Name = "Tipo Balance", Prompt = "Tipo Balance")]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}