using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using AccountMe.Utils;

namespace AccountMe.Extensions
{
    public static class ClaimExtension
    {
        /// <summary>
        ///     Get current user ID
        /// </summary>
        /// <param name="claims"></param>
        /// <returns>int</returns>
        public static int GetSid(this IEnumerable<Claim> claims)
        {
            uint sid;
            try
            {
                sid = Convert.ToUInt32(claims.FirstOrDefault(s => s.Type == ClaimTypes.Sid)?.Value);
            }
            catch
            {
                return default(int);
            }

            return Convert.ToInt32(sid);
        }
        
        /// <summary>
        ///     Get current user username
        /// </summary>
        /// <param name="claims"></param>
        /// <returns>string</returns>
        public static string GetUserName(this IEnumerable<Claim> claims)
        {
            string userName;
            try
            {
                userName = claims.FirstOrDefault(s => s.Type == ClaimTypes.GivenName)?.Value.Trim();
            }
            catch
            {
                return string.Empty;
            }

            return userName;
        }
        
        public static RoleType? GetRole(this IEnumerable<Claim> claims)
        {
            RoleType? currentRole;
            try
            {
                currentRole = claims.FirstOrDefault(s => s.Type == ClaimTypes.Role)?.Value.Trim().ToEnum<RoleType>();
            }
            catch
            {
                return default(RoleType?);
            }

            return currentRole;
        }
        
        
    }
}