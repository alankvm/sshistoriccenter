using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AccountMe.Models;
using AccountMe.Models.AppData;

namespace AccountMe.Controllers
{
    public class Accounts : Controller
    {
        private readonly AppDbContext _context;

        public Accounts(AppDbContext context)
        {
            _context = context;
        }

        // GET: Accounts
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.Accounts.Include(a => a.AccountType).Include(a => a.ChartAccount).Include(a => a.ParentAccount);
            return View(await appDbContext.ToListAsync());
        }

        // GET: Accounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Accounts
                .Include(a => a.AccountType)
                .Include(a => a.ChartAccount)
                .Include(a => a.ParentAccount)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // GET: Accounts/Create
        public IActionResult Create()
        {
            ViewData["IdAccountType"] = new SelectList(_context.AccountTypes, "Id", "Name");
            ViewData["IdChartAccount"] = new SelectList(_context.ChartAccounts, "Id", "Name");
            ViewData["IdParent"] = new SelectList(_context.Accounts, "Id", "Name");
            return View();
        }

        // POST: Accounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Code,IsReadOnly,IdParent,IdChartAccount,IdAccountType,Created,Updated,CreatedBy,UpdatedBy")] Account account)
        {
            if (ModelState.IsValid)
            {
                _context.Add(account);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAccountType"] = new SelectList(_context.AccountTypes, "Id", "Name", account.IdAccountType);
            ViewData["IdChartAccount"] = new SelectList(_context.ChartAccounts, "Id", "Name", account.IdChartAccount);
            ViewData["IdParent"] = new SelectList(_context.Accounts, "Id", "Name", account.IdParent);
            return View(account);
        }

        // GET: Accounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Accounts.FindAsync(id);
            if (account == null)
            {
                return NotFound();
            }
            ViewData["IdAccountType"] = new SelectList(_context.AccountTypes, "Id", "Name", account.IdAccountType);
            ViewData["IdChartAccount"] = new SelectList(_context.ChartAccounts, "Id", "Name", account.IdChartAccount);
            ViewData["IdParent"] = new SelectList(_context.Accounts, "Id", "Name", account.IdParent);
            return View(account);
        }

        // POST: Accounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Code,IsReadOnly,IdParent,IdChartAccount,IdAccountType,Created,Updated,CreatedBy,UpdatedBy")] Account account)
        {
            if (id != account.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(account);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AccountExists(account.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdAccountType"] = new SelectList(_context.AccountTypes, "Id", "Name", account.IdAccountType);
            ViewData["IdChartAccount"] = new SelectList(_context.ChartAccounts, "Id", "Name", account.IdChartAccount);
            ViewData["IdParent"] = new SelectList(_context.Accounts, "Id", "Name", account.IdParent);
            return View(account);
        }

        // GET: Accounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var account = await _context.Accounts
                .Include(a => a.AccountType)
                .Include(a => a.ChartAccount)
                .Include(a => a.ParentAccount)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (account == null)
            {
                return NotFound();
            }

            return View(account);
        }

        // POST: Accounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var account = await _context.Accounts.FindAsync(id);
            _context.Accounts.Remove(account);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AccountExists(int id)
        {
            return _context.Accounts.Any(e => e.Id == id);
        }
    }
}
