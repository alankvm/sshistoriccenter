using System.Threading.Tasks;
using AccountMe.Models;
using AccountMe.Models.AppData;
using AccountMe.Services;
using Microsoft.AspNetCore.Mvc;

namespace AccountMe.Controllers
{
    public class AccountController : Controller
    {
        private readonly AuthorizationService _authService;
        public AccountController(AppDbContext db)
        {
            _authService = new AuthorizationService(db);
        }
        // GET
        public IActionResult Index()
        {
            return
            View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult Logout()
        {
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Email, Password")] Credentials credentials)
        {
            if (ModelState.IsValid)
            {
                _authService.IsValid(credentials.Email.Trim(), credentials.Password.Trim());
                var user = _authService.GetCurrentUser();
                if (user == null) return RedirectToAction("Login");
                await _authService.SetCookie(HttpContext);
                return RedirectToAction("Index", "Home");
            }
            ModelState.AddModelError("Email", "Invalid Email or Password");
            return RedirectToAction("Login");
        }
    }
}