using System.Linq;
using System.Threading.Tasks;
using AccountMe.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AccountMe.Models;
using AccountMe.Models.AppData;
using AccountMe.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.Query;

namespace AccountMe.Controllers
{
    [Authorize(Roles = "SuperAdmin, MainAccountant")]
    public class BookController : Controller
    {
        private readonly AppDbContext _context;

        public BookController(AppDbContext context)
        {
            ViewData["Title_Simple"] = "Libros";
            _context = context;
        }

        // GET: Book
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.Books.Include(b => b.Company);
            /*
             * Only the main accountant has the capacity to add new books a particular company.
             * therefore companies are filtered by main accountant taken from security context.
             * Do not apply this filter for the super user role 
             */
            if(User.Claims.GetRole() != RoleType.SuperAdmin)
                appDbContext = (IIncludableQueryable<Book, Company>) appDbContext
                    .Where(s => s.Company.IdUser == User.Claims.GetSid());
            return View(await appDbContext.ToListAsync());
        }

        // GET: Book/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // GET: Book/Create
        public IActionResult Create()
        {
            var companies = _context.Companies.Where(c => c.IdUser == User.Claims.GetSid());
            ViewData["IdCompany"] = new SelectList(companies, "Id", "Name");
            return View();
        }

        // POST: Book/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,IdCompany,ReadOnly,Period,Comment")] Book book)
        {
            if (ModelState.IsValid)
            {
                book.CreatedBy = User.Claims.GetUserName();
                book.UpdatedBy = User.Claims.GetUserName();
                _context.Add(book);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCompany"] = new SelectList(_context.Companies, "Id", "Address", book.IdCompany);
            return View(book);
        }

        // GET: Book/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books.FindAsync(id);
            if (book == null)
            {
                return NotFound();
            }
            ViewData["IdCompany"] = new SelectList(_context.Companies, "Id", "Name", book.IdCompany);
            return View(book);
        }

        // POST: Book/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,IdCompany,ReadOnly,Period,Comment")] Book book)
        {
            if (id != book.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    book.UpdatedBy = User.Claims.GetUserName();
                    _context.Update(book);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BookExists(book.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCompany"] = new SelectList(_context.Companies, "Id", "Address", book.IdCompany);
            return View(book);
        }

        // GET: Book/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var book = await _context.Books
                .Include(b => b.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (book == null)
            {
                return NotFound();
            }

            return View(book);
        }

        // POST: Book/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var book = await _context.Books.FindAsync(id);
            _context.Books.Remove(book);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BookExists(int id)
        {
            return _context.Books.Any(e => e.Id == id);
        }
    }
}
