using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AccountMe.Models;
using AccountMe.Models.AppData;

namespace AccountMe.Controllers
{
    public class ChartAccounts : Controller
    {
        private readonly AppDbContext _context;

        public ChartAccounts(AppDbContext context)
        {
            _context = context;
        }

        // GET: ChartAccounts
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.ChartAccounts.Include(c => c.Company);
            return View(await appDbContext.ToListAsync());
        }

        // GET: ChartAccounts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chartAccount = await _context.ChartAccounts
                .Include(c => c.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (chartAccount == null)
            {
                return NotFound();
            }

            return View(chartAccount);
        }

        // GET: ChartAccounts/Create
        public IActionResult Create()
        {
            ViewData["IdCompany"] = new SelectList(_context.Companies, "Id", "Address");
            return View();
        }

        // POST: ChartAccounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,IdCompany,Created,Updated,CreatedBy,UpdatedBy")] ChartAccount chartAccount)
        {
            if (ModelState.IsValid)
            {
                _context.Add(chartAccount);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCompany"] = new SelectList(_context.Companies, "Id", "Address", chartAccount.IdCompany);
            return View(chartAccount);
        }

        // GET: ChartAccounts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chartAccount = await _context.ChartAccounts.FindAsync(id);
            if (chartAccount == null)
            {
                return NotFound();
            }
            ViewData["IdCompany"] = new SelectList(_context.Companies, "Id", "Address", chartAccount.IdCompany);
            return View(chartAccount);
        }

        // POST: ChartAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,IdCompany,Created,Updated,CreatedBy,UpdatedBy")] ChartAccount chartAccount)
        {
            if (id != chartAccount.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(chartAccount);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ChartAccountExists(chartAccount.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdCompany"] = new SelectList(_context.Companies, "Id", "Address", chartAccount.IdCompany);
            return View(chartAccount);
        }

        // GET: ChartAccounts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var chartAccount = await _context.ChartAccounts
                .Include(c => c.Company)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (chartAccount == null)
            {
                return NotFound();
            }

            return View(chartAccount);
        }

        // POST: ChartAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var chartAccount = await _context.ChartAccounts.FindAsync(id);
            _context.ChartAccounts.Remove(chartAccount);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ChartAccountExists(int id)
        {
            return _context.ChartAccounts.Any(e => e.Id == id);
        }
    }
}
