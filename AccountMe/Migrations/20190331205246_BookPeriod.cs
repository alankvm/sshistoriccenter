﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AccountMe.Migrations
{
    public partial class BookPeriod : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.AddColumn<int>(
                name: "Period",
                table: "Books",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 1, new DateTime(2019, 3, 31, 14, 52, 45, 561, DateTimeKind.Local).AddTicks(2806), "alan.alvarenga", "Full administrative power", "Super Admin", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Active", "Created", "CreatedBy", "Email", "FullName", "IdManager", "IdRole", "KnownAs", "Password", "UpdatedBy", "UserName" },
                values: new object[] { 1, true, new DateTime(2019, 3, 31, 14, 52, 45, 562, DateTimeKind.Local).AddTicks(9345), "alan.alvarenga", "alankvm@gmail.com", "Alan Kevin Alvarenga Mejia", null, 1, "Ing. Alvarenga", "5301e31808c425331263ec38913dc015bfd3cbdf3ebbc8849e8cf33c1e426d96", "alan.alvarenga", "alan.alvarenga" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DropColumn(
                name: "Period",
                table: "Books");

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 1, new DateTime(2019, 3, 31, 12, 0, 6, 499, DateTimeKind.Local).AddTicks(9690), "alan.alvarenga", "Full administrative power", "Super Admin", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Active", "Created", "CreatedBy", "Email", "FullName", "IdManager", "IdRole", "KnownAs", "Password", "UpdatedBy", "UserName" },
                values: new object[] { 1, true, new DateTime(2019, 3, 31, 12, 0, 6, 501, DateTimeKind.Local).AddTicks(6314), "alan.alvarenga", "alankvm@gmail.com", "Alan Kevin Alvarenga Mejia", null, 1, "Ing. Alvarenga", "5301e31808c425331263ec38913dc015bfd3cbdf3ebbc8849e8cf33c1e426d96", "alan.alvarenga", "alan.alvarenga" });
        }
    }
}
