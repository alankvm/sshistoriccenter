﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AccountMe.Migrations
{
    public partial class TransactionModelMoneyDataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 1, new DateTime(2019, 4, 6, 22, 21, 45, 378, DateTimeKind.Local).AddTicks(9835), "alan.alvarenga", "Full administrative power", "SuperAdmin", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 2, new DateTime(2019, 4, 6, 22, 21, 45, 379, DateTimeKind.Local).AddTicks(1412), "alan.alvarenga", "Manager role", "MainAccountant", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 3, new DateTime(2019, 4, 6, 22, 21, 45, 379, DateTimeKind.Local).AddTicks(1433), "alan.alvarenga", "Limited user role", "Accountant", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Active", "Created", "CreatedBy", "Email", "FullName", "IdManager", "IdRole", "KnownAs", "Password", "UpdatedBy", "UserName" },
                values: new object[] { 1, true, new DateTime(2019, 4, 6, 22, 21, 45, 380, DateTimeKind.Local).AddTicks(6924), "alan.alvarenga", "alankvm@gmail.com", "Alan Kevin Alvarenga Mejia", null, 1, "Ing. Alvarenga", "5301e31808c425331263ec38913dc015bfd3cbdf3ebbc8849e8cf33c1e426d96", "alan.alvarenga", "alan.alvarenga" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 1, new DateTime(2019, 4, 6, 22, 17, 38, 450, DateTimeKind.Local).AddTicks(4121), "alan.alvarenga", "Full administrative power", "SuperAdmin", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 2, new DateTime(2019, 4, 6, 22, 17, 38, 450, DateTimeKind.Local).AddTicks(5480), "alan.alvarenga", "Manager role", "MainAccountant", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 3, new DateTime(2019, 4, 6, 22, 17, 38, 450, DateTimeKind.Local).AddTicks(5498), "alan.alvarenga", "Limited user role", "Accountant", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Active", "Created", "CreatedBy", "Email", "FullName", "IdManager", "IdRole", "KnownAs", "Password", "UpdatedBy", "UserName" },
                values: new object[] { 1, true, new DateTime(2019, 4, 6, 22, 17, 38, 451, DateTimeKind.Local).AddTicks(9905), "alan.alvarenga", "alankvm@gmail.com", "Alan Kevin Alvarenga Mejia", null, 1, "Ing. Alvarenga", "5301e31808c425331263ec38913dc015bfd3cbdf3ebbc8849e8cf33c1e426d96", "alan.alvarenga", "alan.alvarenga" });
        }
    }
}
