﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AccountMe.Migrations
{
    public partial class ChartAccountNormalization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_AccountTypes_AccountTypeId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_ChartAccounts_ChartAccountId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Accounts_ParentId",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_ChartAccounts_Companies_CompanyId",
                table: "ChartAccounts");

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                table: "ChartAccounts",
                newName: "IdCompany");

            migrationBuilder.RenameIndex(
                name: "IX_ChartAccounts_CompanyId",
                table: "ChartAccounts",
                newName: "IX_ChartAccounts_IdCompany");

            migrationBuilder.RenameColumn(
                name: "ParentId",
                table: "Accounts",
                newName: "IdParent");

            migrationBuilder.RenameColumn(
                name: "ChartAccountId",
                table: "Accounts",
                newName: "IdChartAccount");

            migrationBuilder.RenameColumn(
                name: "AccountTypeId",
                table: "Accounts",
                newName: "IdAccountType");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_ParentId",
                table: "Accounts",
                newName: "IX_Accounts_IdParent");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_ChartAccountId",
                table: "Accounts",
                newName: "IX_Accounts_IdChartAccount");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_AccountTypeId",
                table: "Accounts",
                newName: "IX_Accounts_IdAccountType");

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 1, new DateTime(2019, 4, 6, 18, 48, 21, 11, DateTimeKind.Local).AddTicks(4922), "alan.alvarenga", "Full administrative power", "SuperAdmin", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 2, new DateTime(2019, 4, 6, 18, 48, 21, 11, DateTimeKind.Local).AddTicks(6823), "alan.alvarenga", "Manager role", "MainAccountant", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 3, new DateTime(2019, 4, 6, 18, 48, 21, 11, DateTimeKind.Local).AddTicks(6856), "alan.alvarenga", "Limited user role", "Accountant", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Active", "Created", "CreatedBy", "Email", "FullName", "IdManager", "IdRole", "KnownAs", "Password", "UpdatedBy", "UserName" },
                values: new object[] { 1, true, new DateTime(2019, 4, 6, 18, 48, 21, 13, DateTimeKind.Local).AddTicks(9098), "alan.alvarenga", "alankvm@gmail.com", "Alan Kevin Alvarenga Mejia", null, 1, "Ing. Alvarenga", "5301e31808c425331263ec38913dc015bfd3cbdf3ebbc8849e8cf33c1e426d96", "alan.alvarenga", "alan.alvarenga" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_AccountTypes_IdAccountType",
                table: "Accounts",
                column: "IdAccountType",
                principalTable: "AccountTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_ChartAccounts_IdChartAccount",
                table: "Accounts",
                column: "IdChartAccount",
                principalTable: "ChartAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Accounts_IdParent",
                table: "Accounts",
                column: "IdParent",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ChartAccounts_Companies_IdCompany",
                table: "ChartAccounts",
                column: "IdCompany",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_AccountTypes_IdAccountType",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_ChartAccounts_IdChartAccount",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_Accounts_Accounts_IdParent",
                table: "Accounts");

            migrationBuilder.DropForeignKey(
                name: "FK_ChartAccounts_Companies_IdCompany",
                table: "ChartAccounts");

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.RenameColumn(
                name: "IdCompany",
                table: "ChartAccounts",
                newName: "CompanyId");

            migrationBuilder.RenameIndex(
                name: "IX_ChartAccounts_IdCompany",
                table: "ChartAccounts",
                newName: "IX_ChartAccounts_CompanyId");

            migrationBuilder.RenameColumn(
                name: "IdParent",
                table: "Accounts",
                newName: "ParentId");

            migrationBuilder.RenameColumn(
                name: "IdChartAccount",
                table: "Accounts",
                newName: "ChartAccountId");

            migrationBuilder.RenameColumn(
                name: "IdAccountType",
                table: "Accounts",
                newName: "AccountTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_IdParent",
                table: "Accounts",
                newName: "IX_Accounts_ParentId");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_IdChartAccount",
                table: "Accounts",
                newName: "IX_Accounts_ChartAccountId");

            migrationBuilder.RenameIndex(
                name: "IX_Accounts_IdAccountType",
                table: "Accounts",
                newName: "IX_Accounts_AccountTypeId");

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 1, new DateTime(2019, 4, 6, 18, 24, 17, 576, DateTimeKind.Local).AddTicks(9324), "alan.alvarenga", "Full administrative power", "SuperAdmin", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 2, new DateTime(2019, 4, 6, 18, 24, 17, 580, DateTimeKind.Local).AddTicks(443), "alan.alvarenga", "Manager role", "MainAccountant", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Created", "CreatedBy", "Description", "Name", "UpdatedBy" },
                values: new object[] { 3, new DateTime(2019, 4, 6, 18, 24, 17, 580, DateTimeKind.Local).AddTicks(663), "alan.alvarenga", "Limited user role", "Accountant", "alan.alvarenga" });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Active", "Created", "CreatedBy", "Email", "FullName", "IdManager", "IdRole", "KnownAs", "Password", "UpdatedBy", "UserName" },
                values: new object[] { 1, true, new DateTime(2019, 4, 6, 18, 24, 17, 617, DateTimeKind.Local).AddTicks(7181), "alan.alvarenga", "alankvm@gmail.com", "Alan Kevin Alvarenga Mejia", null, 1, "Ing. Alvarenga", "5301e31808c425331263ec38913dc015bfd3cbdf3ebbc8849e8cf33c1e426d96", "alan.alvarenga", "alan.alvarenga" });

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_AccountTypes_AccountTypeId",
                table: "Accounts",
                column: "AccountTypeId",
                principalTable: "AccountTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_ChartAccounts_ChartAccountId",
                table: "Accounts",
                column: "ChartAccountId",
                principalTable: "ChartAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Accounts_Accounts_ParentId",
                table: "Accounts",
                column: "ParentId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ChartAccounts_Companies_CompanyId",
                table: "ChartAccounts",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
