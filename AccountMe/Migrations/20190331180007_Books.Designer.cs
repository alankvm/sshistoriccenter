﻿// <auto-generated />
using System;
using AccountMe.Models.AppData;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace AccountMe.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20190331180007_Books")]
    partial class Books
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.3-servicing-35854")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("AccountMe.Models.Book", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment");

                    b.Property<DateTime>("Created")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<int>("IdCompany");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<bool>("ReadOnly");

                    b.Property<DateTime>("Updated")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("IdCompany");

                    b.ToTable("Books");
                });

            modelBuilder.Entity("AccountMe.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasMaxLength(250);

                    b.Property<string>("Comment");

                    b.Property<DateTime>("Created")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(150);

                    b.Property<int>("IdUser");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<string>("Phone")
                        .IsRequired()
                        .HasMaxLength(12);

                    b.Property<DateTime>("Updated")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("IdUser");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("AccountMe.Models.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Created")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<string>("Description")
                        .HasMaxLength(240);

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.Property<DateTime>("Updated")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.ToTable("Roles");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Created = new DateTime(2019, 3, 31, 12, 0, 6, 499, DateTimeKind.Local).AddTicks(9690),
                            CreatedBy = "alan.alvarenga",
                            Description = "Full administrative power",
                            Name = "Super Admin",
                            Updated = new DateTime(2019, 3, 31, 12, 0, 6, 500, DateTimeKind.Local).AddTicks(29),
                            UpdatedBy = "alan.alvarenga"
                        });
                });

            modelBuilder.Entity("AccountMe.Models.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Active");

                    b.Property<DateTime>("Created")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy")
                        .HasMaxLength(50);

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(190);

                    b.Property<string>("FullName")
                        .IsRequired()
                        .HasMaxLength(200);

                    b.Property<int?>("IdManager");

                    b.Property<int>("IdRole");

                    b.Property<string>("KnownAs")
                        .HasMaxLength(70);

                    b.Property<string>("Password")
                        .IsRequired()
                        .HasMaxLength(65);

                    b.Property<DateTime>("Updated")
                        .ValueGeneratedOnAddOrUpdate();

                    b.Property<string>("UpdatedBy")
                        .HasMaxLength(50);

                    b.Property<string>("UserName")
                        .IsRequired()
                        .HasMaxLength(50);

                    b.HasKey("Id");

                    b.HasIndex("IdManager");

                    b.HasIndex("IdRole");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Active = true,
                            Created = new DateTime(2019, 3, 31, 12, 0, 6, 501, DateTimeKind.Local).AddTicks(6314),
                            CreatedBy = "alan.alvarenga",
                            Email = "alankvm@gmail.com",
                            FullName = "Alan Kevin Alvarenga Mejia",
                            IdRole = 1,
                            KnownAs = "Ing. Alvarenga",
                            Password = "5301e31808c425331263ec38913dc015bfd3cbdf3ebbc8849e8cf33c1e426d96",
                            Updated = new DateTime(2019, 3, 31, 12, 0, 6, 501, DateTimeKind.Local).AddTicks(6324),
                            UpdatedBy = "alan.alvarenga",
                            UserName = "alan.alvarenga"
                        });
                });

            modelBuilder.Entity("AccountMe.Models.Book", b =>
                {
                    b.HasOne("AccountMe.Models.Company", "Company")
                        .WithMany("Books")
                        .HasForeignKey("IdCompany")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("AccountMe.Models.Company", b =>
                {
                    b.HasOne("AccountMe.Models.User", "MainAccountant")
                        .WithMany("Companies")
                        .HasForeignKey("IdUser")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("AccountMe.Models.User", b =>
                {
                    b.HasOne("AccountMe.Models.User", "Manager")
                        .WithMany("Employees")
                        .HasForeignKey("IdManager");

                    b.HasOne("AccountMe.Models.Role", "Role")
                        .WithMany("Users")
                        .HasForeignKey("IdRole")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
