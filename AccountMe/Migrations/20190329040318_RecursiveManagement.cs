﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AccountMe.Migrations
{
    public partial class RecursiveManagement : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                "IdManager",
                "Users",
                nullable: true);

            migrationBuilder.CreateIndex(
                "IX_Users_IdManager",
                "Users",
                "IdManager");

            migrationBuilder.AddForeignKey(
                "FK_Users_Users_IdManager",
                "Users",
                "IdManager",
                "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                "FK_Users_Users_IdManager",
                "Users");

            migrationBuilder.DropIndex(
                "IX_Users_IdManager",
                "Users");

            migrationBuilder.DropColumn(
                "IdManager",
                "Users");
        }
    }
}