namespace AccountMe.Utils
{
    /// <summary>
    /// User defined roles
    /// </summary>
    public enum RoleType
    {
        NotDefined,
        SuperAdmin,
        MainAccountant,
        Accountant
    }
}