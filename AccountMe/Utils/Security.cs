using System;
using System.Security.Cryptography;
using System.Text;

namespace AccountMe.Utils
{
    public static class Security
    {
        public static string Hash(string phrase)
        {
            if (string.IsNullOrEmpty(phrase)) return string.Empty;
            using (var sha256 = SHA256.Create())
            {
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(phrase));
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }
    }
}